# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.6-super](https://gitlab.com/rumble-o-bin/neextjs/compare/v0.1.5-super...v0.1.6-super) (2022-04-22)

### [0.1.5-super](https://gitlab.com/rumble-o-bin/neextjs/compare/v0.1.4-super...v0.1.5-super) (2022-04-22)

### [0.1.4-super](https://gitlab.com/rumble-o-bin/neextjs/compare/v0.1.3-super...v0.1.4-super) (2022-04-22)

### [0.1.3-super](https://gitlab.com/rumble-o-bin/neextjs/compare/v0.1.2-super...v0.1.3-super) (2022-04-22)

### [0.1.2-super](https://gitlab.com/rumble-o-bin/neextjs/compare/v0.1.2...v0.1.2-super) (2022-04-22)

### [0.1.2](https://gitlab.com/rumble-o-bin/neextjs/compare/v0.0.2-super...v0.1.2) (2022-04-22)

### 0.1.1 (2022-04-22)


### Features

* play k8s ([3084d3b](https://gitlab.com/rumble-o-bin/neextjs/commit/3084d3ba797a0de53777116a8567c4ba34a96852))
* play k8s ([b5ce4fa](https://gitlab.com/rumble-o-bin/neextjs/commit/b5ce4fafa30e34462483ff054adc05235383fb26))
